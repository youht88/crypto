import 'dart:convert';
import 'dart:io';

import 'package:crypto/util/crypto_util.dart';
import 'package:flutter/material.dart';
import 'package:pointycastle/pointycastle.dart' as crypto;
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:zefyrka/zefyrka.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String msg = "";
  String _data = "";
  ZefyrController _controller = ZefyrController();
  late IO.Socket socket;
  _MyHomePageState() {
    socket = IO.io(
        'http://127.0.0.1:8849',
        IO.OptionBuilder()
            .setTransports(['websocket'])
            //.enableForceNewConnection()
            //.enableAutoConnect()
            .enableReconnection()
            .disableAutoConnect()
            .build());
    socket.connect();
    socket.onConnect((_) {
      print('client-->connect');
    });
    socket.on('event', (data) => print("client-->event:$data"));
    socket.onDisconnect((_) => print('client-->disconnect'));
  }
  void _clear() {
    _data = "";
    setState(() {});
  }

  void _httpSignature() async {
    msg = _controller.document.toPlainText();
    msg = json.encode(_controller.document.toDelta().toJson());
    print(msg);
    final client = HttpClient();
    final HttpClientRequest req =
        await client.get("127.0.0.1", 8848, "/signature?msg=$msg");
    final HttpClientResponse res = await req.close();
    _data = await res.transform(utf8.decoder).join();
    setState(() {});
  }

  void _socketSignature() async {
    final msgBuffer =
        utf8.encode(json.encode(_controller.document.toDelta().toJson()));
    socket.emitWithAck('signature', msgBuffer, ack: (data) {
      _data = data;
      setState(() {});
    }, binary: true);
  }

  void _socketGetAccount() async {
    socket.emitWithAck('getAccount', "", ack: (data) {
      _data = data;
      setState(() {});
    });
  }

  void _safeSendWithECDH() async {
    final msg = json.encode(_controller.document.toDelta().toJson());
    final String remotePublicKeyHexStr = CryptoLib.genSM2KeyPair()['publicKey'];
    final dataMap = {
      "msg": msg,
      "remotePublicKeyHexStr": remotePublicKeyHexStr,
      "type": 'txt',
      "algType": 'sm2'
    };
    socket.emitWithAck('safeSendWithECDH', utf8.encode(json.encode(dataMap)),
        ack: (data) {
      _data = data;
      setState(() {});
    }, binary: true);
  }

  void _getAuthHeader() async {
    socket.emitWithAck('getAuthBase64Header', "", ack: (data) {
      _data = data;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: [
                  Column(
                    children: [
                      ZefyrToolbar.basic(controller: _controller),
                      Container(
                        height: 200,
                        child: Container(
                          color: Colors.lightBlue,
                          child: ZefyrEditor(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            controller: _controller,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Wrap(
                      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      spacing: 4,
                      runSpacing: 4,
                      children: [
                        ElevatedButton(
                            onPressed: () => _clear(), child: Text("清除")),
                        ElevatedButton(
                            onPressed: () => _httpSignature(),
                            child: Text("http模式签名")),
                        ElevatedButton(
                            onPressed: () => _socketSignature(),
                            child: Text("socket模式签名")),
                        ElevatedButton(
                            onPressed: () => _socketGetAccount(),
                            child: Text("获取账户")),
                        ElevatedButton(
                            onPressed: () => _safeSendWithECDH(),
                            child: Text("ECDH安全发送数据")),
                        ElevatedButton(
                            onPressed: () => _getAuthHeader(),
                            child: Text("获得AuthHeader")),
                      ]),
                ],
              ),
              SizedBox(height: 4),
              SelectableText("$_data"),
            ],
          ),
        ),
      ),
    );
  }
}
